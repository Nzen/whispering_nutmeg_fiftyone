
## Whispering Nutmeg FiftyOne ##

A library to copy stuff around, without installing ant or gradle.

If you know of better software that's relatively light, I can try that. However, I'm constrained to use java or a windows shell (dos or powershell), as I can't oblige the hosts to install cygwin for other runtimes.

### Run instruction ###

Required libraries :  
* Alibaba [fastjson](http://search.maven.org/#artifactdetails%7Ccom.alibaba%7Cfastjson%7C1.2.28%7Cjar) 1.2.28
* Apache [Commons Io](http://commons.apache.org/proper/commons-io/download_io.cgi) 2.5
* Apache [Commons Cli](http://commons.apache.org/proper/commons-cli/download_cli.cgi) 1.4

1 (Prepare wn51_config with the relevant artifacts, paths, and projects.)
2 java -jar wn51.jar (--verbose)

Whispering Nutmeg 51 will then try to apply the recipe tasks with the available artifacts. Which is to say, backup from living to the designated backup folder; and, update from that folder to the projects with just the artifacts it needs.

Arguments:  
* -h - show arguments, ie this list
* -v --verbose - show more words
* -b [[path]] - override the backup folder path in wn51's config json
* -u [[path]] - override the update folder path in wn51's config json

&copy; Nicholas Prado, released under 2-clause BSD license terms.

