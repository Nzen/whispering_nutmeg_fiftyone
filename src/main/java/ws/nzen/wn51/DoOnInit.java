/** see ../../../../../../../LICENSE for release rights */
package ws.nzen.wn51;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;

import com.alibaba.fastjson.JSON;

import ws.nzen.wn51.model.Artifact;
import ws.nzen.wn51.model.Config;
import ws.nzen.wn51.model.Location;
import ws.nzen.wn51.model.Project;
import ws.nzen.wn51.model.TomcatWebapp;

/** Until I decide the real point to hang this on
 */
public class DoOnInit
{
	private static final String cl = "doi."; 
	static final String backupFlag = "b", updateFlag = "u", 
			verboseFlag = "v", helpFlag = "h";
	private boolean verbose;

	public static void main( String[] args )
	{
		Options knowsCliDtd = prepCliParser();
		CommandLine userInput = prepCli( knowsCliDtd, args );
		DoOnInit doesStuff = prepDoer( userInput );
		String defaultJsonConfigFilename = "wn51_config.json";
		Config knowsStuff = doesStuff.buildConfigFrom(
				doesStuff.getJsonOfConfig(
						defaultJsonConfigFilename ) );
		String currentlySupportedVersion = "001";
		if ( knowsStuff.getApiVersion().compareTo( currentlySupportedVersion ) != 0 )
		{
			System.err.println( "only config version "
					+currentlySupportedVersion +" currently supported" );
			return;
		}
		knowsStuff = alterWithCli( userInput, doesStuff, knowsStuff );
		Map<Path,Artifact> knowsFiles = doesStuff.getAvailable( knowsStuff );
		for ( String task : knowsStuff.getRecipeTasks() )
		{
			if ( task.equals( "backup" ) )
			{
				doesStuff.makeBackups( knowsStuff, knowsFiles );
			}
			else if ( task.equals( "update" ) )
			{
				doesStuff.replaceOldVersions( knowsStuff, knowsFiles );
			}
		}
	}


	public static Options prepCliParser( )
	{
		Options knowsCliDtd = new Options();
		final boolean needsEmbelishment = true;
		knowsCliDtd.addOption( backupFlag, needsEmbelishment, "Backup folder path"
				+ " (ex C:\\Program Files\\apache tomcat\\7.0.73)" );
		knowsCliDtd.addOption( updateFlag, needsEmbelishment, "Update folder path"
				+ " (ex /home/theusername/tmp)" );
		knowsCliDtd.addOption( verboseFlag, "verbose", ! needsEmbelishment,
				"show more words" );
		knowsCliDtd.addOption( helpFlag, "show arg flags" );
		return knowsCliDtd;
	}


	public static CommandLine prepCli(
			Options knowsCliDtd, String[] args )
	{
		CommandLineParser cliRegex = new DefaultParser();
		CommandLine userInput = null;
		try
		{
			userInput = cliRegex.parse( knowsCliDtd, args );
			if ( userInput.hasOption( helpFlag ) )
			{
				new HelpFormatter().printHelp( "Whispering Nutmeg 51", knowsCliDtd );
			}
		}
		catch ( ParseException pe )
		{
			System.err.println( cl +"pc just using config: couldn't parse input "+ pe );
		}
		return userInput;
	}


	public static DoOnInit prepDoer( CommandLine userInput )
	{
		DoOnInit doesStuff;
		if ( userInput != null && userInput.hasOption( verboseFlag ) )
		 {
			boolean wantsLogging = true;
			doesStuff = new DoOnInit( wantsLogging );
		 }
		else
		{
			 doesStuff = new DoOnInit();
		}
		return doesStuff;
	}


	public static Config alterWithCli( CommandLine userInput,
			DoOnInit doesStuff, Config knowsStuff )
	{
		if ( userInput != null )
		{
			if ( userInput.hasOption( backupFlag ) )
			{
				String sessionBackup = userInput.getOptionValue( backupFlag );
				if ( sessionBackup != null )
				{
					Location pathsOfSession = knowsStuff.getPaths();
					if ( doesStuff.verbose )
					{
						System.out.println( cl +"m() replacing backup path: old - "
								+ pathsOfSession.getBackup() +"\n\twith - "+ sessionBackup );
					}
					pathsOfSession.setBackup( sessionBackup );
					knowsStuff.setPaths( pathsOfSession );
				}
			}
			if ( userInput.hasOption( updateFlag ) )
			{
				String sessionUpdate = userInput.getOptionValue( updateFlag );
				if ( sessionUpdate != null )
				{
					Location pathsOfSession = knowsStuff.getPaths();
					if ( doesStuff.verbose )
					{
						System.out.println( cl +"m() replacing update path: old - "
								+ pathsOfSession.getBackup() +"\n\twith - "+ sessionUpdate );
					}
					pathsOfSession.setBackup( sessionUpdate );
					knowsStuff.setPaths( pathsOfSession );
				}
			}
		}
		return knowsStuff;
	}


	public DoOnInit()
	{
		this( false );
	}


	public DoOnInit( boolean noiseTolerance )
	{
		verbose = noiseTolerance;
	}


	private Config buildConfigFrom( String jsonAlready )
	{
		return JSON.parseObject( jsonAlready, Config.class );
	}


	private String getJsonOfConfig( String configFilename )
	{
		String here = cl +"gjfc ";
		StringBuilder fileContent = new StringBuilder();
		String temp;
		try ( BufferedReader br = new BufferedReader(
				new FileReader( Paths.get( configFilename ).toFile() ) ); )
		{
			while ( true )
			{
				temp = br.readLine();
				if ( temp != null )
				{
					fileContent.append( temp + "\n" );
				}
				else
				{
					break; // eof
				}
			}
		}
		catch ( FileNotFoundException fnfe )
		{
			System.err.println( here +"no config file"
					+ " because "+ fnfe );
		}
		catch ( IOException ie )
		{
			System.err.println( here +"inaccessible config file"
					+ " because "+ ie );
		}
		return fileContent.toString();
	}


	private Map<Path,Artifact> getAvailable( Config describes )
	{
		final String here = cl +"ga ";
		System.out.print( here +"Available: " );
		Map<Path,Artifact> present = new java.util.HashMap<>();
		for ( Artifact currFileDesc : describes.getArtifacts() )
		{
			try
			{
				Path currPath = Paths.get( describes.getPaths().getUpdate()
						+ File.separator + currFileDesc.getFname() );
				if ( currPath.toFile().exists() )
				{
					present.put( currPath, currFileDesc );
					System.out.print( currFileDesc.getFname() +" ;; " );
				}
			}
			catch ( InvalidPathException | SecurityException ioe )
			{
				System.err.println( here + currFileDesc.getFname()
						+" is not accessible because "+ ioe );
			}
		}
		System.out.println();
		return present;
	}


	private void makeBackups( Config toHere,
			Map<Path,Artifact> withThese )
	{
		final String here = cl +"mb ";
		final boolean targetIsDir = true;
		try
		{
			Path backupDir = Paths.get( toHere.getPaths().getBackup() );
			for ( Path currFile : withThese.keySet() )
			{
				String currCopyFile = currFile.getFileName().toString();
				boolean found = false;
				String artifactId = withThese.get( currFile ).getaId();
				for ( Project currDir : toHere.getProjects() )
				{
					for ( String wantedId : currDir.getWants() )
					{
						if ( artifactId.equals( wantedId ) )
						{
							try
							{
								File presentVersion = referredBy( currDir,
										currFile, withThese.get( currFile ),
										toHere, ! targetIsDir ).toFile();
								File targetFolder = backupDir.toFile();
								if ( verbose )
								{
									System.out.println( here +"about to back up "+ presentVersion
											+"\n\tinto folder "+ targetFolder );
								}
								FileUtils.copyFileToDirectory( presentVersion,
										targetFolder );
								System.out.println( here +"backed up "+ currCopyFile );
								found = true;
								break;
							}
							catch ( IOException ie )
							{
								System.err.println( here +"problem copying "
										+ currCopyFile +"\n\tfrom "
										+ currDir.getpId() +" because "+ ie );
								ie.printStackTrace();
							}
						}
					}
					if ( found )
					{
						break; // we only need to backup one copy
					}
				}
			}
		}
		catch ( InvalidPathException ipe )
		{
			System.err.println( here +" backup folder "
					+ toHere.getPaths().getBackup()
					+" is not accessible because "+ ipe );
		}
	}


	/** path to file or null on failure */
	private Path referredBy( Project dirRef, Path fileToFind,
			Artifact fileDesc, Config allInfo, boolean targetIsDir )
	{
		final String here = cl +"rb ";
		String place = "";
		String artifactFolder = "";
		if ( dirRef.getType().equals( Project.TYPE_INSTALLATION ) )
		{
			artifactFolder = allInfo.getPaths().getInstallation();
			if ( fileDesc.getFtype().equals( Artifact.TYPE_CONFIG ) )
			{
				artifactFolder += File.separator +"config";
			}
			else if ( fileDesc.getFtype().equals( Artifact.TYPE_JAR ) )
			{
				artifactFolder += File.separator +"riseLib";
			}
		}
		else if ( dirRef.getType().equals( Project.TYPE_WEBAPP ) )
		{
			artifactFolder = allInfo.getPaths().getTomcat()
					+ File.separator +"webapps"
					+ File.separator + webappReferredBy( dirRef.getpId(), allInfo )
					+ File.separator +"WEB-INF";
			if ( fileDesc.getFtype().equals( Artifact.TYPE_CONFIG ) )
			{
				artifactFolder += File.separator +"classes";
			}
			else if ( fileDesc.getFtype().equals( Artifact.TYPE_JAR ) )
			{
				artifactFolder += File.separator +"lib";
			}
		}
		try
		{
			if ( targetIsDir )
			{
				place = artifactFolder;
			}
			else
			{
				place = artifactFolder
						+ File.separator + fileToFind.getFileName();
			}
			return Paths.get( place );
		}
		catch ( InvalidPathException ipe )
		{
			System.err.println( here +"path to artifact "
					+ place +" is not accessible because "+ ipe );
			return null;
		}
	}


	/** your dir name or blank */
	private String webappReferredBy( String projectId, Config allInfo )
	{
		final String here = cl +"wrb ";
		for ( TomcatWebapp candidate : allInfo.getTomcatWebapps() )
		{
			if ( candidate.getwId().equals( projectId ) )
			{
				return candidate.getDirName();
			}
		}
		System.err.println( here +"project id "+ projectId
				+" not among the webapp ids, sending blank" ); // throw exception?
		return "";
	}


	private void replaceOldVersions( Config toHere,
			Map<Path,Artifact> withThese )
	{
		final String here = cl +"mb ";
		final boolean targetIsDir = true;
		try
		{
			for ( Path currFile : withThese.keySet() )
			{
				String currCopyFile = currFile.getFileName().toString();
				String artifactId = withThese.get( currFile ).getaId();
				for ( Project currDir : toHere.getProjects() )
				{
					for ( String wantedId : currDir.getWants() )
					{
						if ( artifactId.equals( wantedId ) )
						{
							try
							{
								File newestVersion = currFile.toFile();
								File folderHasDeprecatedVersion = referredBy(
										currDir, currFile, withThese.get( currFile ),
											toHere, targetIsDir
										).toFile();
								if ( verbose )
								{
									System.out.println( here +"about to send "+ newestVersion
											+"\n\tinto folder "+ folderHasDeprecatedVersion );
								}
								FileUtils.copyFileToDirectory( newestVersion, folderHasDeprecatedVersion );
								System.out.println( here + "updated "+ currDir
										.getpId() +"'s "+ currCopyFile );
								break;
							}
							catch ( IOException ie )
							{
								System.err.println( here +"problem copying "
										+ currCopyFile +"\n\tfrom "
										+ currDir.getpId() +" because "+ ie );
								ie.printStackTrace();
							}
						}
					}
				}
			}
		}
		catch ( InvalidPathException ipe )
		{
			System.err.println( here +" update folder "
					+ toHere.getPaths().getBackup()
					+" is not accessible because "+ ipe );
		}
	}


}




















