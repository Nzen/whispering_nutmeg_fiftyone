/**
 * 
 */
package ws.nzen.wn51.model;

import com.alibaba.fastjson.JSON;

/**
 * currently a struct
 */
public class Config
{
	protected String apiVersion;
	/** the artifacts that an update could involve */
	protected Artifact[] artifacts;
	/** paths to the typical folders we use */
	protected Location paths;
	/** webapp folders within tomcat we may copy artifacts to */
	protected TomcatWebapp[] tomcatWebapps;
	/**  */
	protected Project[] projects;
	/** not sure if I'll use this, but the stuff to do, and the order */ 
	protected String[] recipeTasks;


	public static Config fromJson( String json )
	{
		return JSON.parseObject( json, Config.class );
	}


	/** backups should preceed updates */
	private void ensureSensibleRecipeOrdering()
	{
		if ( recipeTasks == null )
		{
			return;
		}
		int backInd = -1, updInd = -1, currInd = -1;
		for ( String task : recipeTasks  )
		{
			currInd++;
			if ( task == null )
			{
				continue;
			}
			else if ( task.equals( "backup" ) )
			{
				backInd = currInd;
				continue;
			}
			else if ( task.equals( "update" ) )
			{
				updInd = currInd;
				continue;
			}
		}
		if ( backInd >= 0 && updInd >= 0
				&& updInd < backInd )
		{
			String tempTask = recipeTasks[ updInd ];
			recipeTasks[ updInd ] = recipeTasks[ backInd ];
			recipeTasks[ backInd ] = tempTask;
		}
	}


	public String getApiVersion()
	{
		return apiVersion;
	}


	public void setApiVersion( String apiVersion )
	{
		this.apiVersion = apiVersion;
	}


	public Artifact[] getArtifacts()
	{
		return artifacts;
	}


	public void setArtifacts( Artifact[] artifacts )
	{
		this.artifacts = artifacts;
	}


	public Location getPaths()
	{
		return paths;
	}


	public void setPaths( Location paths )
	{
		this.paths = paths;
	}


	public TomcatWebapp[] getTomcatWebapps()
	{
		return tomcatWebapps;
	}


	public void setTomcatWebapps( TomcatWebapp[] tomcatWebapps )
	{
		this.tomcatWebapps = tomcatWebapps;
	}


	public Project[] getProjects()
	{
		return projects;
	}


	public void setProjects( Project[] projects )
	{
		this.projects = projects;
	}


	public String[] getRecipeTasks()
	{
		ensureSensibleRecipeOrdering();
		return recipeTasks;
	}


	public void setRecipeTasks( String[] recipeTasks )
	{
		this.recipeTasks = recipeTasks;
	}

}











