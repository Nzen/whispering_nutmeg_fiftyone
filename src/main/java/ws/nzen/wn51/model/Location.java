/** see ../../../../../../../LICENSE for release rights */
package ws.nzen.wn51.model;

/** currently a struct
 */
public class Location
{
	protected String installation;
	protected String tomcat;
	protected String update;
	protected String backup;

	public String getInstallation()
	{
		return installation;
	}
	public void setInstallation( String installation )
	{
		this.installation = installation;
	}
	public String getTomcat()
	{
		return tomcat;
	}
	public void setTomcat( String tomcat )
	{
		this.tomcat = tomcat;
	}
	public String getUpdate()
	{
		return update;
	}
	public void setUpdate( String update )
	{
		this.update = update;
	}
	public String getBackup()
	{
		return backup;
	}
	public void setBackup( String backup )
	{
		this.backup = backup;
	}

}




















