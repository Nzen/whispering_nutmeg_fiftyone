/** see ../../../../../../../LICENSE for release rights */
package ws.nzen.wn51.model;

/**
 * currently a struct
 */
public class Project
{
	public static String TYPE_INSTALLATION = "installation";
	public static String TYPE_WEBAPP = "tomcat";
	protected String pId;
	protected String type;
	protected String[] wants;

	public String getpId()
	{
		return pId;
	}
	public void setpId( String pId )
	{
		this.pId = pId;
	}
	public String getType()
	{
		return type;
	}
	public void setType( String type )
	{
		this.type = type;
	}
	public String[] getWants()
	{
		return wants;
	}
	public void setWants( String[] wants )
	{
		this.wants = wants;
	}
}




















