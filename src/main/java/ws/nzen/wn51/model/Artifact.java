/** see ../../../../../../../LICENSE for release rights */
package ws.nzen.wn51.model;

/** currently a struct
 */
public class Artifact
{
	public static String TYPE_JAR = "jar";
	public static String TYPE_CONFIG = "config";
	protected String aId;
	protected String ftype;
	protected String fname;

	public String getaId()
	{
		return aId;
	}
	public void setaId( String aId )
	{
		this.aId = aId;
	}
	public String getFtype()
	{
		return ftype;
	}
	public void setFtype( String ftype )
	{
		this.ftype = ftype;
	}
	public String getFname()
	{
		return fname;
	}
	public void setFname( String fname )
	{
		this.fname = fname;
	}


}




















